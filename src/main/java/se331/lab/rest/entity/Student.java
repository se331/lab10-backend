package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String studentId;
    String name;
    String surname;
    Double gpa;
    String image;
    Integer penAmount;
    String description;
    @ManyToOne
    @JsonBackReference
    Lecturer advisor;
    @ManyToMany(mappedBy = "students")
    @Builder.Default
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonBackReference
    List<Course> enrolledCourses = new ArrayList<Course>();
}

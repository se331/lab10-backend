package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;

import java.util.List;

@Service
@Slf4j
public class CourseServiceImpl implements CourseService{
    @Autowired
    CourseDao courseDao;
    @Override
    public List<Course> getAllCourses() {
        log.info("service received called");
        List<Course> courses = courseDao.getAllCourses();
        log.info("service received courses from dao");
        return courses;
    }
}

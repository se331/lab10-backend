package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.repository.LecturerRepository;

import java.util.List;
@Repository
@Slf4j
public class LecturerDaoImpl implements LecturerDao {
    @Autowired
    LecturerRepository lecturerRepository;

    @Override
    public List<Lecturer> getAllLecturer() {
        log.info("find all lecturer in db");
        return lecturerRepository.findAll();
    }
}
